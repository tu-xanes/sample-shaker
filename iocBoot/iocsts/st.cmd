#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

#epicsEnvSet("IOCBL", "XNS")
#epicsEnvSet("IOCDEV", "PHY")


## macros
epicsEnvSet("BL", "$(IOCBL)")
epicsEnvSet("DEV", $(IOCDEV")

## Load record instances
dbLoadRecords("${TOP}/iocBoot/${IOC}/XNS_Shaker_ctrl.db", "BL=$(IOCBL), DEV=$(IOCDEV), CH=m8")
dbLoadRecords("${TOP}/iocBoot/${IOC}/XNS_Shaker_ctrl_cond.db", "BL=$(IOCBL), DEV=$(IOCDEV), CH=m8")
dbLoadRecords("${TOP}/iocBoot/${IOC}/XNS_Shaker_timer.db", "BL=$(IOCBL), DEV=$(IOCDEV), CH=m8")
dbLoadRecords("${TOP}/iocBoot/${IOC}/XNS_Shaker_StepRegime.db", "BL=$(IOCBL), DEV=$(IOCDEV), CH=m8")


cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
